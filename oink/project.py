import click
import os

import oink.utils as ocu


def get_project_by_name_or_id(name_or_id):
    pass


@click.group()
def project():
    """Project management commands"""
    pass


@project.command()
@click.option("-p", "--project",
              prompt="Project Name",
              help="New project name",
              required=True,
              default=lambda: os.path.basename(os.getcwd()),
              type=str)
def new(project):
    """Create a new project"""
    if not ocu.is_oink_setup():
        ocu.print_error("Oink has not been setup. Please run [bold]oink config[/bold].")
        return

    ocu.print_pig()

    ocu.print_info("Creating new project at %s." % os.getcwd())
    prev_project = ocu.OinkProject.load(os.getcwd(), traverse=False)
    if prev_project is not None:
        ocu.print_error("The project [bold]%s[/bold] already exists in this folder." % prev_project.name)
        return
    opf = ocu.OinkProject(project, os.getcwd())
    opf.save()
    ocu.print_success("Project [bold]%s[/bold] created successfully." % opf.name)
