import click
import oink.utils as ocu


@click.command()
@click.option("-n", "--name",
              prompt="Your Name",
              help="Your name, for documentation",
              required=True,
              default=lambda: ocu.get_oink_config_value("user_name", None),
              type=str)
@click.option("-u", "--unicode",
              prompt="Unicode Support",
              help="Whether or not your terminal supports unicode",
              required=True,
              default=lambda: ocu.get_oink_config_value("unicode", True),
              type=bool)
@click.option("-e", "--emojis",
              prompt="Emoji Support",
              help="Whether or not your terminal supports emojis",
              required=True,
              default=lambda: ocu.get_oink_config_value("emojis", True),
              type=bool)
@click.option("-o", "--order",
              prompt=True,
              help="Default Sort Order for Tasks",
              default=lambda: ocu.get_oink_config_value("order", "ASC"),
              type=click.Choice(["ASC", "DESC"], case_sensitive=False))
@click.option("-s", "--status",
              prompt=True,
              help="Default Status Filter for Tasks",
              default=lambda: ocu.get_oink_config_value("status", "OPEN"),
              type=click.Choice(["ALL", "OPEN", "DONE", "FLAG", "TODO"], case_sensitive=False))
def config(name, unicode, emojis, order, status):
    """Configure local Oink Task settings"""
    ocu.print_pig()
    ocu.print_info("Configuring local Oink setup.")
    ocu.write_oink_config_value("user_name", name)
    ocu.write_oink_config_value("unicode", str(unicode))
    ocu.write_oink_config_value("emojis", str(emojis))
    ocu.write_oink_config_value("order", order)
    ocu.write_oink_config_value("status", status)
    ocu.print_success("Oink configuration saved.")
