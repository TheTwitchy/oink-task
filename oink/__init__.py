__version__ = "1.0.1"

# Application global vars
PROG_NAME = "oink"
PROG_DESC = "Project-oriented task tracking for weirdos."
PROG_EPILOG = "Written by TheTwitchy"
DEBUG = True
