import click
import os
import rich
from rich.table import Table

import oink.utils as ocu


def get_task_by_desc_or_id(search, tasks):
    search_by_id = False
    try:
        # Try to turn the input into an int
        input_id = int(search)
        search_by_id = True
    except ValueError:
        search_by_id = False

    if not search_by_id:
        possible_tasks = []
        for task in tasks:
            if search in task.description:
                possible_tasks.append(task)

        if len(possible_tasks) > 1:
            ocu.print_error("The input '[bold]%s[/bold]'' matches several tasks. "
                            "Please give a more specific description." % search)
            return None
        elif len(possible_tasks) == 0:
            ocu.print_error("The input '[bold]%s[/bold]'' did not match any tasks. " % search)
            return None
        input_id = possible_tasks[0].id

    for task in tasks:
        # Find the correct task
        if input_id == task.id:
            return task


@click.command()
@click.option("-t", "--task",
              prompt="Task Description",
              help="New task description",
              required=True,
              type=str)
@click.option("-s", "--status",
              prompt=False,
              help="New task status",
              required=True,
              default="TODO",
              type=click.Choice(["DONE", "FLAG", "TODO"], case_sensitive=False))
def new(task, status):
    """Create a new task"""
    if not ocu.is_oink_setup():
        ocu.print_error("Oink has not been setup. Please run [bold]oink config[/bold].")
        return

    project = ocu.OinkProject.load(os.getcwd())
    if project is None:
        ocu.print_error("There is no project in this directory tree.")
        return

    ocu.print_info("Creating new task for [bold]%s[/bold]." % project.name)
    new_task_obj = ocu.OinkTask(task, status=status)

    project.add_task(new_task_obj)
    project.save()

    ocu.print_success("New task created successfully.")


def get_status_style(status):
    try:
        if ocu.get_oink_config_value("emojis").lower() == "true":
            if status.upper() == "TODO":
                return "[bold yellow]⚠️[/bold yellow]"
            if status.upper() == "DONE":
                return "[bold green]✔️[/bold green]"
            if status.upper() == "FLAG":
                return "[bold red]🏴[/bold red]"
        elif ocu.get_oink_config_value("unicode").lower() == "true":
            if status.upper() == "TODO":
                return "[bold yellow]\u25a0[/bold yellow]"
            if status.upper() == "DONE":
                return "[bold green]\u2714[/bold green]"
            if status.upper() == "FLAG":
                return "[bold red]\u2691[/bold red]"
        else:
            # Sadness.
            raise KeyError
    except (KeyError, AttributeError):
        if status.upper() == "TODO":
            return "[bold yellow]TODO[/bold yellow]"
        if status.upper() == "DONE":
            return "[bold green]DONE[/bold green]"
        if status.upper() == "FLAG":
            return "[bold red]FLAG[/bold red]"


@click.command()
@click.option("-s", "--status",
              prompt=False,
              help="Filter by task status",
              default=lambda: ocu.get_oink_config_value("status", "ALL"),
              type=click.Choice(["DONE", "FLAG", "TODO", "OPEN", "ALL"], case_sensitive=False))
@click.option("-o", "--order",
              prompt=False,
              default=lambda: ocu.get_oink_config_value("order", "ASC"),
              help="Order tasks by creation date",
              type=click.Choice(["ASC", "DESC"], case_sensitive=False))
def tasks(status, order):
    """Show all tasks"""
    if not ocu.is_oink_setup():
        ocu.print_error("Oink has not been setup. Please run [bold]oink config[/bold].")
        return

    project = ocu.OinkProject.load(os.getcwd())
    if project is None:
        ocu.print_error("There is no project in this directory tree.")
        return

    ocu.print_info("[bold]%s[/bold] Tasks:" % project.name)
    table = Table(show_header=True, header_style="bold")
    table.add_column("ID", style="dim", justify="right")
    table.add_column("Status", style="", justify="right")
    table.add_column("Descriptor", style="", justify="left")
    table.box = rich.box.SIMPLE_HEAD

    if order.upper() == "DESC":
        task_array = project.tasks[::-1]
    else:
        task_array = project.tasks

    for task in task_array:
        if status != "ALL":
            if status == "OPEN" and task.status.upper() != "DONE":
                table.add_row(str(task.id), get_status_style(task.status), task.description)
            else:
                if task.status.upper() == status.upper():
                    table.add_row(str(task.id), get_status_style(task.status), task.description)
        else:
            table.add_row(str(task.id), get_status_style(task.status), task.description)
    ocu.console.print(table)


@click.command()
@click.option("-t", "--task",
              prompt="Task",
              help="Task Description or ID",
              required=True,
              type=str)
@click.option("-s", "--status",
              prompt=False,
              help="Task status update",
              type=click.Choice(["DONE", "FLAG", "TODO"], case_sensitive=False))
def status(task, status):
    """Show and/or update task status"""
    if not ocu.is_oink_setup():
        ocu.print_error("Oink has not been setup. Please run [bold]oink config[/bold].")
        return

    project = ocu.OinkProject.load(os.getcwd())
    if project is None:
        ocu.print_error("There is no project in this directory tree.")
        return

    task_obj = get_task_by_desc_or_id(task, project.tasks)
    if task_obj is None:
        return

    ocu.print_info("[bold]%s[/bold] Task:" % project.name)
    table = Table(show_header=True, header_style="bold")
    table.add_column("ID", style="dim", justify="right")
    table.add_column("Status", style="", justify="right")
    table.add_column("Descriptor", style="", justify="left")
    table.box = rich.box.SIMPLE_HEAD
    table.add_row(str(task_obj.id), get_status_style(task_obj.status), task_obj.description)
    ocu.console.print(table)

    try:
        # See if the user wants to update the status
        if not status:
            new_status = click.prompt(
                    "Set status",
                    default=task_obj.status,
                    type=click.Choice(["DONE", "FLAG", "TODO"], case_sensitive=False))
        else:
            new_status = status

        if not new_status.upper() == task_obj.status.upper():
            old_status = task_obj.status
            task_obj.status = new_status
            project.save()
            ocu.print_success("Updated task from %s to %s." %
                              (get_status_style(old_status), get_status_style(new_status)))
    except click.Abort:
        return


@click.command()
def edit():
    """Edit a project or task by opening the current project file in an editor"""
    if not ocu.is_oink_setup():
        ocu.print_error("Oink has not been setup. Please run [bold]oink config[/bold].")
        return

    project = ocu.OinkProject.load(os.getcwd())
    if project is None:
        ocu.print_error("There is no project in this directory tree.")
        return

    ocu.print_info("Opening project file for [bold]%s[/bold]." % project.name)
    print(project.get_fq_project_file())
    click.launch(project.get_fq_project_file())
