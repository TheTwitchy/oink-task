# Oink Task

![Oink Logo](/docs/imgs/pig.png)

Project-oriented task tracking for weirdos.

## Features
* Autodetects your current project based on your working directory or the first parent directory that has a project.
* Entirely local, no external connections.
* Tasks are stored in Markdown format for easy reading, even without `oink`.
* Three different levels of task status.
* Emojis and unicode in your terminal (for those cool enough to support it).

## Installation
### Standard
* `python3 setup.py install --user`
* `oink --help`
* `oink config`

### Development
* It is recommended that you use a virtualenv for development:
    * `virtualenv --python python3 venv`
    * `source ./venv/bin/activate`
* `python setup.py develop`
* Run with `oink`

## FAQs
* Q: What's an example of a workflow this enables?
    * A: You could go to your root user folder (like `~/`) and drop a "Misc" project as a catch-all for non-project specific things you need to do. Then, in a project folder (like `~/projects/project_ABC/`) you can drop more specific projects. That way, anytime you work in the project folder (or any of it's subfolders) it will autodetect the correct project, and any other work will drop into the "Misc" project.
* Q: How and where are tasks stored?
    * A: In markdown files in each folder where you create a project. Look for the `.oink.md` file.
* Q: How do I move, edit, or delete tasks or projects?
    * A: In the `.oink.md` file for the project, just change it accordingly. There are no other storage mechanisms.

## Usage
### Help
* Add `--help` anywhere to get documentation on how to use `oink` commands.

![Help](/docs/imgs/help.png "Help")

### Config
* This command sets local variables in your installation. You are required to run this before using `oink`.

![Config](/docs/imgs/config.png "Config")

### Project
* This command creates local projects, under which tasks will be grouped. Ensure you are running this from the top-most level of the project folder you want to associate these tasks with. You are required to run this before creating `oink` tasks.

![Project New](/docs/imgs/project_new.png "Project New")


### New Task
* This commands creates new tasks. You can optionally set the status, but TODO is set if not given.

![New Task](/docs/imgs/new.png "New Task")


### Tasks
* This commands lists tasks. You can optionally filter by status.

![Tasks](/docs/imgs/tasks.png "Tasks")

### Task Status
* This commands gets and updates task status.

![Task Status](/docs/imgs/status.png "Task Status")
