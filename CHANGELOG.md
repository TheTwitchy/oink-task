# Changelog

<!--next-version-placeholder-->

## v1.0.1 (2021-05-14)
### Fix
* Drop python min version to 3.6 ([`aef9cd4`](https://gitlab.com/TheTwitchy/oink-task/-/commit/aef9cd40946ab02485e6ea2ad151b207264f36b6))
